#include <string.h>

/*DÉFINITION*/

/* Prend en paramètre le #code d'identification de l'erreur
 * Affiche le message suivant : "Une erreur est survenue : code #######."
 * */
void afficherMessageErreur(FILE* fichierLog, char codeErreur[5]);

/*(Fonction interne) Écrit dans le fichier des logs le message entré en paramètre */
int ecrireMessageLogs(FILE* fichierLog, char* message);

/*Crée un nouveau fichier unique dans le même dossier que main.c
 * Type de nommage #time en secondes : ####_journal_de_logs.txt
 * Retourne 0 en cas d'erreur sinon retourne 1*/
FILE * initialisationJournal();

/*ACTION*/

void afficherMessageErreur(FILE* fichierLog, char codeErreur[5]){
    char messageErreur[185] = "/!\\ Une erreur est survenue : code ";
    strcat(messageErreur,codeErreur);
    strcat(messageErreur, ". Consultez la rubrique \"Erreurs CGUITA:\" pour plus d'informations (https://www.notion.so/mneveu/CGuita-9a6bbe0fed684126b5c34a424e86df9a). /!\\");
    printf(messageErreur, codeErreur);
    ecrireMessageLogs(fichierLog,messageErreur);
}

int ecrireMessageLogs(FILE* fichierLog, char* message){
    if (fichierLog != NULL){
        char dateAffichee[1000];
        time_t dateActuelle = time(NULL);

        struct tm * p = localtime(&dateActuelle);

        strftime(dateAffichee, 1000, "%d/%m/%Y %H:%M:%S : ", p);

        fputs(dateAffichee, fichierLog);
        fputs(message,fichierLog);
        fputs("\n",fichierLog);
        return 1;
    }
    else{
        afficherMessageErreur(fichierLog,"-4");
        return 0;
    }
}

FILE * initialisationJournal(){
//  Nommage
    time_t secondes;
    char strSecondes[11];
    char nomFichier[40];
    secondes = time(NULL);
    sprintf(strSecondes,"%ld",secondes);
    strcat(nomFichier, strSecondes);
    strcat(nomFichier, "_journal_de_logs");
    strcat(nomFichier, ".txt");

//  Création du fichier
    FILE * logs = fopen("journal_de_logs.txt", "w");
//    FILE* logs = fopen(nomFichier,"w");
    if (logs){
        printf("Fichier du journal de logs créé avec succès.\n");
    }else {
        afficherMessageErreur(logs,"-3");
        return 0;
    }

//  Message d'initialisation
    ecrireMessageLogs(logs, "Lancement du programme..");
    return logs;
}
