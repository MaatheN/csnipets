#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

// BOOLUSERANSWERLISTENER()
// return a translation of a user 'yes or not' answer by a boolean value (or -1 error)
// So the user can answer in English or French :
// -> 1 : y / Y / Yes / YES / yes / O / o / oui / OUI / carriage return
// -> 0 : n / N / No / NO / no / Non / NON
// -> -1 : other
int boolUserAnswerListener(){
    char boolUserAnswer[1] = {'\0','\0'};
    scanf(" %1c", boolUserAnswer);
    int boolStatus=-1;
    if (boolUserAnswer[0] == 'o' || boolUserAnswer[0] == 'O' || boolUserAnswer[0] == 'y' || boolUserAnswer[0] == 'Y') {
        return boolStatus = 1;
    }else if (boolUserAnswer[0] == 'n' || boolUserAnswer[0] == 'N'){
        return boolStatus = 0;
    }else
        return boolStatus;
}
