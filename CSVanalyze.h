/*DÉFINITION*/

/*Analyse basique du fichier et inscription dans les logs :
 * - Nombre de lignes
 * - Nombre de colonnes */
int * indiquerCsvMetas(FILE* fichierCsv, FILE* fichierLogs);

/* Renvoie des tableaux* (clients) de tableaux[5] (infos clients) :
 * - NUMERO CLIENT
 * - CODE POSTAL
 * - NOM
 * - ADRESSE
 * - VILLE
 **/
char ** listerDonneesClient(FILE* fichierCsv, FILE* logs);

/*ACTION*/

int * indiquerCsvMetas(FILE* fichierCsv, FILE* fichierLogs){
    int iLignes = 1;
    int iColonnes = 1;
    static int res[2];
    char caractere=0;

    fichierCsv = fopen("export.csv", "r");

    if (fichierCsv != NULL){
        while ((caractere=fgetc(fichierCsv)) != EOF){
            if (iLignes==1 && caractere ==';'){
                iColonnes++;
            }
            if (caractere=='\n'){
                iLignes++;
            }
        }
        char sLignes[9], sColonnes[9];
        sprintf(sLignes,"%d",iLignes);
        sprintf(sColonnes,"%d",iColonnes);
        char messageLignes[55] = "Nombre de lignes présentes dans le fichier : ";
        strcat(messageLignes,sLignes);
        char messageColonnes[58] = "Nombre de colonnes présentes dans le fichier : ";
        strcat(messageColonnes,sColonnes);
        printf("%s\n", messageLignes);
        printf("%s\n", messageColonnes);
        ecrireMessageLogs(fichierLogs, messageLignes);
        ecrireMessageLogs(fichierLogs, messageColonnes);

        res[0] = iLignes;
        res[1] = iColonnes;
        fclose(fichierCsv);
        return res;
    }
    else{
        afficherMessageErreur(fichierLogs, "21");
        ecrireMessageLogs(fichierLogs, "Fin du programme.");
        return 0;
    }
}

char ** listerDonneesClient(FILE* fichierCsv, FILE* logs) {

    int *metasCsv;
    metasCsv=indiquerCsvMetas(fichierCsv,logs);
    int nbrClients = metasCsv[0]-1;
    char caractere = 0;
    int compteurPointVirgule = 0;
    int seqCharid = 0;
    int ligneCounter = 0;

    char **idsClient = NULL;
    char **donneesClient = NULL;

    fichierCsv = fopen("export.csv", "r");

    if (fichierCsv != NULL){
        while ((caractere=fgetc(fichierCsv)) != EOF){
            if (ligneCounter > 0){
//                printf("Caractère en cours d'analyse : %c\n",caractere);
                if (caractere==';'){
                    compteurPointVirgule++;
                    seqCharid = 0;
                    printf("4");
                }else if (caractere=='\n'){
                    ligneCounter++;
                    compteurPointVirgule = 0;
                }else if (compteurPointVirgule == 0){
                    idsClient[ligneCounter-1][seqCharid] = caractere;
                    seqCharid++;
//                    printf("Caractère identifié comme ID client : %c \n", caractere);
                    printf("Tableau des ids : %c", idsClient[ligneCounter-1][seqCharid]);
                }
            }else if (caractere=='\n'){
                ligneCounter++;
            }
        }
        fclose(fichierCsv);
    }
    size_t n = sizeof(idsClient);
    printf("Taille du tableau idClient : %ld\n", n);
    return donneesClient;
}